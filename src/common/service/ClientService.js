import axios from 'axios';
import qs from 'qs'

class ClientService {

    get(api, params) {
        params = params || {};
        return new Promise((resolve, reject) => {
            axios.get(api, qs.stringify(params))
                 .then(res => {
                    if (res.data.status === 0) {
                        resolve(res.data);
                    } else {
                        alert(res.data.msg)
                    }
                 })
                 .catch(error => {
                        alert('网络异常');
                 })
        })
    }

    post(api, params) {
        params = params || {};
        return new Promise((resolve, reject) => {
            axios.post(api, qs.stringify(params))
                 .then(res => {
                    if (res.data.status === 0) {
                        resolve(res.data);
                    } else {
                        alert(res.data.msg);
                    }
                 })
                 .catch(error => {
                    alert('网络异常');
                 })
        })
    }

}